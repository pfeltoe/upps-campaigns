$( document ).ready(function() {
 
    $( "a.read-more" ).click(function( event ) {
 
        $( ".feature-info", $(this).parent().parent() ).slideToggle( 500 );
 
        event.preventDefault();
 
    });
 
});